# 编辑待办

> **编辑幂等性**
>
> 原水门户会使用`clientId`和`thirdPartId`去做幂等性，即如果该终端调用该接口，那么针对每条待办会有如下2种情况：
>
> 1. 如果`thirdPartId`在原水门户中 _存在_，则会进行编辑
> 2. 如果`thirdPartId`在原水门户中 _不存在_，则不会进行任何操作。
>
{style="note"}

> `{{host}}`请替换为原水门户服务地址
>
{style="tip"}

- **请求路径** `{{host}}/home/todo/edit`

- **请求方式** `POST`

- **请求头**

| 字段            | 必填 | 备注                |
|---------------|----|-------------------|
| Authorization | Y  | `{{accessToken}}` |

- **请求内容**

| 字段                | 类型       | 必填 | 备注                |
|-------------------|----------|----|-------------------|
| list              | object[] | Y  | 批量列表              |
| list.thirdPartId  | string   | Y  | 第三方id             |
| list.type         | string   |    | 类型：0-待办 1-已办 2-抄送 |
| list.title        | string   |    | 待办标题              |
| list.content      | string   |    | 待办内容              |
| list.receiverUser | string   |    | 执行人/接收人账号         |
| list.read         | boolean  |    | 是否已读，默认为未读        |
| list.url          | string   |    | 待办跳转链接            |

- 请求示例：

```json lines
{
  "list": [
    {
      "thirdPartId": "123123",
      "type": "0",
      "title": "@Validated@Validated",
      "content": "待办内容",
      "receiverUser": "admin",
      "receiverTime": "2024-01-01 00:00:00",
      "read": true,
      "url": "http://127.0.0.1:10010/test-api/todo?id=1"
    }
  ]
}
```

- **响应内容**: 空

- 响应示例:

```json lines
{
  "msg": "操作成功",
  "code": 200,
}
```