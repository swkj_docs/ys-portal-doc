# 获取accessToken

创建好终端后，需要使用终端的 `clientId` 和 `secret` 来获取授权 `accessToken`。

> `{{host}}`请替换为原水门户服务地址
>
{style="tip"}

- **请求路径** `{{host}}/system/auth/accessToken`

- **请求方式** `POST`

- **请求内容**

| 字段         | 类型     | 必填 | 备注                  |
|------------|--------|----|---------------------|
| clientId   | string | Y  | 终端id                |
| timestamp  | long   | Y  | 时间戳                 |
| salt       | string | Y  | 加盐的随机值              |
| verifyCode | string | Y  | 授权校验码(下文会解释如何生成校验码) |

> 关于文档中的空值
> - **必填**项为空时，表示为*不必填*
> - **非空**项为空时，表示为*可以为空*

{style="note"}

- 请求示例：

```json lines
{
  "clientId": "123",
  "timestamp": 111222,
  "salt": "123",
  "verifyCode": "HomxFXTY1Rj0AwPKMwQwH5Yml3xUoyHIrkTog6q4B00p5T6EecFVfaTin7l5qeXNUZxz5JkIU7n+fuUCUj3IlnyKYuYBs1lHh3CotkfMfZpOBtd/qx+oJoWT+VTytbG5TX2Aq/2Sb1T7khP+rX6RP/X8OiLq4i+MJ6SC0g6wvus="
}
```

- **响应内容**

| 字段        | 类型     | 非空 | 备注          |
|-----------|--------|----|-------------|
| token     | string | Y  | accessToken |
| timestamp | long   | Y  | token到期的时间戳 |

- 响应示例:

```json lines
{
  "msg": "操作成功",
  "code": 200,
  "data": {
    "token": "eyJhbGciOiJIUzUxMiJ9.eyJsb2dpbl91c2VyX2tleSI6IjlhNzhkYzYyLTU1NmUtNGViYS1hMzU3LWU3MTE0MzIzZWE1NyJ9.vj5UT9uGlGv8whVuksfBjATpwdyZXsUvcpf7T--7zg5E6kamk4JICKshFu8SmWeUhO2-EYDdAUBDp3p7jZ0BuA",
    "timestamp": 1705411749786
  }
}
```

