# 终端获取用户token



> `{{host}}`请替换为原水门户服务地址
>
{style="tip"}

- **请求路径** `{{host}}/loginWithClient`

- **请求方式** `POST`

- **请求内容**

| 字段       | 类型     | 必填 | 备注  |
|----------|--------|----|-----|
| userName | string | Y  | 用户名 | 

- 请求示例：

```json lines
{
  "userName": "admin"
}
```

- **响应内容**:

| 字段    | 类型     | 非空 | 备注    |
|-------|--------|----|-------|
| token | string | Y  | token |

- 响应示例:

```json lines
{
  "msg": "操作成功",
  "code": 200,
  "data": {
    "token": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
  }
}
```